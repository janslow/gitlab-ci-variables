# GitLab CI Variables

A package which parses the [predefined variables](https://docs.gitlab.com/ce/ci/variables/README.html) set by GitLab CI, providing a structured object (with TypeScript definitions) representing the data from the environment.

## Installation
```bash
npm install gitlab-ci-variables
# or 
yarn add gitlab-ci-variables
```

## Usage
```typescript
import loadVariables, { isGitLabCI } from 'gitlab-ci-variables';

if (isGitLabCI()) {
  console.log('Running under GitLab CI runner');
}

const variables = loadVariables();
// variables === undefined iff !isGitLabCI()
if (variables) {
  console.log(`Build (ID ${variables.build.id}) of ${variables.project.name}. Started by ${variables.user.email}.`);
}
```

See the packaged TypeScript definitions for a full list of properties. Alternatively, see the `Variables` interface in [the source code](https://gitlab.com/janslow/gitlab-ci-variables/blob/develop/src/index.ts).
