export interface Variables {
  readonly server: {
    /**
     * The name of CI server that is used to coordinate jobs. CI_SERVER_NAME.
     */
    readonly name: string;
    /**
     * GitLab version that is used to schedule jobs. CI_SERVER_VERSION.
     */
    readonly version: string;
    /**
     * GitLab revision that is used to schedule jobs. CI_SERVER_REVISION.
     */
    readonly revision: string;
  };
  readonly commit: {
    /**
     * The commit revision for which project is built. CI_COMMIT_SHA.
     */
    readonly sha: string;
    /**
     * The commit tag name. Present only when building tags. CI_COMMIT_TAG.
     */
    readonly tag?: string;
    /**
     * The branch or tag name for which project is built. CI_COMMIT_REF_NAME.
     */
    readonly ref: string;
    /**
     * ref lowercased, shortened to 63 bytes, and with everything except 0-9 and a-z replaced with -. No leading / trailing -.
     * Use in URLs, host names and domain names.
     * CI_COMMIT_REF_SLUG
     */
    readonly refSlug: string;
  };
  readonly job: {
    /**
     * The unique id of the current job that GitLab CI uses internally. CI_JOB_ID.
     */
    readonly id: number;
    /**
     * The name of the job as defined in .gitlab-ci.yml. CI_JOB_NAME.
     */
    readonly name: string;
    /**
     * The name of the stage as defined in .gitlab-ci.yml. CI_JOB_STAGE.
     */
    readonly stage: string;
    /**
     * The flag to indicate that job was manually started. CI_JOB_MANUAL.
     */
    readonly manual: boolean;
    /**
     * Token used for authenticating with the GitLab Container Registry. CI_JOB_TOKEN.
     */
    readonly token: string;
  };
  readonly pipeline: {
    /**
     * The unique id of the current pipeline that GitLab CI uses internally. CI_PIPELINE_ID.
     */
    readonly id: number;
    /**
     * The flag to indicate that job was triggered. CI_PIPELINE_TRIGGERED.
     */
    readonly triggered: boolean;
  };
  readonly project: {
    /**
     * The unique id of the current project that GitLab CI uses internally. CI_PROJECT_ID.
     */
    readonly id: number;
    /**
     * The project name that is currently being built (actually it is project folder name). CI_PROJECT_NAME.
     */
    readonly name: string;
    /**
     * The project namespace (username or groupname) that is currently being built. CI_PROJECT_NAMESPACE.
     */
    readonly namespace: string;
    /**
     * The namespace with project name. CI_PROJECT_PATH.
     */
    readonly path: string;
    /**
     * The HTTP address to access project. CI_PROJECT_URL.
     */
    readonly url: string;
    /**
     * The full path where the repository is cloned and where the job is run. CI_PROJECT_DIR.
     */
    readonly dir: string;
    /**
     * The URL to clone the Git repository. CI_REPOSITORY_URL.
     */
    readonly repo: string;
  };
  readonly registry?: {
    /**
     * If the Container Registry is enabled it returns the address of GitLab's Container Registry. CI_REGISTRY.
     */
    readonly registry: string;
    /**
     * If the Container Registry is enabled for the project it returns the address of the registry tied to the specific project.
     * CI_REGISTRY_IMAGE.
     */
    readonly image?: string;
  };
  readonly environment?: {
    /**
     * The name of the environment for this job. CI_ENVIRONMENT_NAME.
     */
    readonly name: string;
    /**
     * A simplified version of the environment name, suitable for inclusion in DNS, URLs, Kubernetes labels, etc. CI_ENVIRONMENT_SLUG.
     */
    readonly slug: string;
  };
  readonly runner: {
    /**
     * The unique id of runner being used. CI_RUNNER_ID.
     */
    readonly id: number;
    /**
     * The description of the runner as saved in GitLab. CI_RUNNER_DESCRIPTION.
     */
    readonly description: string;
    /**
     * The defined runner tags. CI_RUNNER_TAGS.
     */
    readonly tags: string[];
  };
  /**
   * Whether debug tracing is enabled. CI_DEBUG_TRACE.
   */
  readonly debug: boolean;
  readonly user: {
    /**
     * The id of the user who started the build. GITLAB_USER_ID.
     */
    readonly id: number;
    /**
     * The email of the user who started the build. GITLAB_USER_EMAIL.
     */
    readonly email: string;
  };
}

interface Env {
  // CI: string;
  CI_COMMIT_REF_NAME: string;
  CI_COMMIT_REF_SLUG: string;
  CI_COMMIT_SHA: string;
  CI_COMMIT_TAG?: string;
  // CI_CONFIG_PATH: string;
  CI_DEBUG_TRACE?: string;
  CI_ENVIRONMENT_NAME?: string;
  CI_ENVIRONMENT_SLUG?: string;
  // CI_ENVIRONMENT_URL?: string;
  CI_JOB_ID: string;
  CI_JOB_MANUAL?: string;
  CI_JOB_NAME: string;
  CI_JOB_STAGE: string;
  CI_JOB_TOKEN: string;
  CI_REPOSITORY_URL: string;
  CI_RUNNER_ID: string;
  CI_RUNNER_DESCRIPTION: string;
  CI_RUNNER_TAGS: string;
  CI_PIPELINE_ID: string;
  // CI_PIPELINE_SOURCE?: string;
  CI_PIPELINE_TRIGGERED?: string;
  CI_PROJECT_DIR: string;
  CI_PROJECT_ID: string;
  CI_PROJECT_NAME: string;
  CI_PROJECT_NAMESPACE: string;
  CI_PROJECT_PATH: string;
  // CI_PROJECT_PATH_SLUG: string;
  CI_PROJECT_URL: string;
  CI_REGISTRY?: string;
  CI_REGISTRY_IMAGE?: string;
  // CI_REGISTRY_PASSWORD?: string;
  // CI_REGISTRY_USER?: string;
  CI_SERVER: string;
  CI_SERVER_NAME: string;
  CI_SERVER_REVISION: string;
  CI_SERVER_VERSION: string;
  // ARTIFACT_DOWNLOAD_ATTEMPTS: string;
  // GET_SOURCES_ATTEMPTS: string;
  GITLAB_CI: string;
  GITLAB_USER_ID: string;
  GITLAB_USER_EMAIL: string;
  // RESTORE_CACHE_ATTEMPTS: string;
}
export function isGitLabCI(): boolean {
  return process.env.GITLAB_CI === 'true' && process.env.CI_SERVER === 'yes';
}

export default function load(): Variables | undefined {
  if (!isGitLabCI()) {
    return undefined;
  }
  const env: Env = process.env as any;
  return {
    server: {
      name: env.CI_SERVER_NAME,
      revision: env.CI_SERVER_REVISION,
      version: env.CI_SERVER_VERSION,
    },
    commit: {
      ref: env.CI_COMMIT_REF_NAME,
      refSlug: env.CI_COMMIT_REF_SLUG,
      sha: env.CI_COMMIT_SHA,
      tag: env.CI_COMMIT_TAG,
    },
    job: {
      id: parseInt(env.CI_JOB_ID, 10),
      manual: parseBool(env.CI_JOB_MANUAL),
      name: env.CI_JOB_NAME,
      stage: env.CI_JOB_STAGE,
      token: env.CI_JOB_TOKEN,
    },
    pipeline: {
      id: parseInt(env.CI_PIPELINE_ID, 10),
      triggered: parseBool(env.CI_PIPELINE_TRIGGERED),
    },
    project: {
      dir: env.CI_PROJECT_DIR,
      id: parseInt(env.CI_PROJECT_ID, 10),
      name: env.CI_PROJECT_NAME,
      namespace: env.CI_PROJECT_NAMESPACE,
      path: env.CI_PROJECT_PATH,
      url: env.CI_PROJECT_URL,
      repo: env.CI_REPOSITORY_URL,
    },
    debug: parseBool(env.CI_DEBUG_TRACE),
    registry: env.CI_REGISTRY === undefined ? undefined : {
      registry: env.CI_REGISTRY,
      image: env.CI_REGISTRY_IMAGE,
    },
    environment: env.CI_ENVIRONMENT_NAME === undefined ? undefined : {
      name: env.CI_ENVIRONMENT_NAME,
      slug: env.CI_ENVIRONMENT_SLUG as string,
    },
    runner: {
      id: parseInt(env.CI_RUNNER_ID, 10),
      description: env.CI_RUNNER_DESCRIPTION,
      tags: (env.CI_RUNNER_TAGS || '').split(',').map(x => x.trim()).filter(x => x.length > 0),
    },
    user: {
      id: parseInt(env.GITLAB_USER_ID, 10),
      email: env.GITLAB_USER_EMAIL,
    },
  };
}

function parseBool(raw: string | undefined): boolean {
  if (raw === undefined) {
    return false;
  }
  raw = raw.toLowerCase();
  return raw === 'true' || raw === 'yes';
}
