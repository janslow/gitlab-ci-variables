import 'mocha';
import { should, expect } from 'chai';

should();

import loadVariables, { Variables, isGitLabCI } from './index';

function getPropertyName(getter: (obj: any) => any): string {
  const parts: PropertyKey[] = [];
  function createProxy(): Object {
    return new Proxy({}, {
      get: (_, name) => {
        parts.push(name);
        return createProxy();
      },
    });
  }
  getter(createProxy());
  return parts.join('.');
}

function cleanEnv(): void {
  let env: any;
  beforeEach(() => {
    env = process.env;
    process.env = {};
  });
  afterEach(() => {
    process.env = env;
    env = undefined;
  });
}

describe('isGitLabCI()', () => {
  cleanEnv();
  let env: any;
  beforeEach(() => {
    env = {
      CI_SERVER: 'yes',
      GITLAB_CI: 'true',
    };
    process.env = env;
  });

  it(`returns true if CI_SERVER env variable is 'yes' and GITLAB_CI variable is 'true'`, () => {
    isGitLabCI().should.be.true;
  });
  it(`returns false if GITLAB_CI variable is not 'true'`, () => {
    env.GITLAB_CI = 'false';
    isGitLabCI().should.be.false;
    env.GITLAB_CI = 'yes';
    isGitLabCI().should.be.false;
    env.GITLAB_CI = 'no';
    isGitLabCI().should.be.false;
  });
  it(`returns false if CI_SERVER variable is not 'yes'`, () => {
    env.CI_SERVER = 'false';
    isGitLabCI().should.be.false;
    env.CI_SERVER = 'true';
    isGitLabCI().should.be.false;
    env.CI_SERVER = 'no';
    isGitLabCI().should.be.false;
  });
});

describe('load()', () => {
  cleanEnv();
  it('returns undefined if not in a GitLab CI environment.', () => {
    expect(loadVariables()).to.equal(undefined);
  });
  describe('returns a parsed object if in a CI environment', () => {
    let env: any;
    let currentSection: string;
    beforeEach(() => {
      env = {
        CI_SERVER: 'yes',
        GITLAB_CI: 'true',
      };
      process.env = env;
      currentSection = '';
    });

    function getPropertyTestSectionName(getter: (obj: any) => any): string {
      let name = getPropertyName(getter);
      if (name.startsWith(currentSection)) {
        return name.substring(currentSection.length);
      }
      return `.${name}`;
    }

    function getVariables(): Variables {
      const vars = loadVariables();
      expect(vars).to.be.an('object');
      return vars as Variables;
    }

    function describeStringProperty(getter: (vars: Variables) => string | undefined, v: string, checkUndefined = true): void {
      describe(getPropertyTestSectionName(getter), () => {
        it(`is the value of ${v}.`, () => {
          const expected = `my-string-for-${v}`;
          env[v] = expected;
          expect(getter(getVariables())).to.equal(expected);
        });
        if (checkUndefined) {
          it(`is undefined if ${v} is not defined.`, () => {
            expect(getter(getVariables())).to.equal(undefined);
          });
        }
      });
    }
    function describeNumericProperty(getter: (vars: Variables) => number, v: string): void {
      describe(getPropertyTestSectionName(getter), () => {
        it(`is the numeric value of the ${v}.`, () => {
          env[v] = '1234';
          expect(getter(getVariables())).to.equal(1234);
          env[v] = '0';
          expect(getter(getVariables())).to.equal(0);
        });
      });
    }
    function describeBooleanProperty(getter: (vars: Variables) => boolean, v: string): void {
      describe(getPropertyTestSectionName(getter), () => {
        it(`is true if ${v} is 'true'.`, () => {
          env[v] = 'true';
          expect(getter(getVariables())).to.be.true;
        });
        it(`is true if ${v} is 'yes'.`, () => {
          env[v] = 'yes';
          expect(getter(getVariables())).to.be.true;
        });
        it(`is false if ${v} is undefined.`, () => {
          expect(getter(getVariables())).to.be.false;
        });
        it(`is false if ${v} is 'false'.`, () => {
          env[v] = 'false';
          expect(getter(getVariables())).to.be.false;
        });
        it(`is false if ${v} is 'no'.`, () => {
          env[v] = 'no';
          expect(getter(getVariables())).to.be.false;
        });
      });
    }
    function describeOptionalSection(getter: (vars: Variables) => Object | undefined, v: string, spec: () => void): void {
      describe(getPropertyTestSectionName(getter), () => {
        beforeEach(() => {
          env[v] = `my-placeholder-string-for-${v}`;
        });
        it(`is defined if ${v} is defined.`, () => {
          expect(getter(getVariables())).to.be.an('object');
        });
        it(`is undefined if ${v} is undefined.`, () => {
          delete env[v];
          expect(getter(getVariables())).to.be.undefined;
        });
        const oldSection = currentSection;
        currentSection = getPropertyName(getter);
        spec();
        currentSection = oldSection;
      });
    }

    describeStringProperty(vars => vars.server.name, 'CI_SERVER_NAME');
    describeStringProperty(vars => vars.server.version, 'CI_SERVER_VERSION');
    describeStringProperty(vars => vars.server.revision, 'CI_SERVER_REVISION');
    describeStringProperty(vars => vars.commit.sha, 'CI_COMMIT_SHA');
    describeStringProperty(vars => vars.commit.tag, 'CI_COMMIT_TAG');
    describeStringProperty(vars => vars.commit.ref, 'CI_COMMIT_REF_NAME');
    describeStringProperty(vars => vars.commit.refSlug, 'CI_COMMIT_REF_SLUG');
    describeNumericProperty(vars => vars.job.id, 'CI_JOB_ID');
    describeStringProperty(vars => vars.job.name, 'CI_JOB_NAME');
    describeStringProperty(vars => vars.job.stage, 'CI_JOB_STAGE');
    describeBooleanProperty(vars => vars.job.manual, 'CI_JOB_MANUAL');
    describeStringProperty(vars => vars.job.token, 'CI_JOB_TOKEN');
    describeNumericProperty(vars => vars.pipeline.id, 'CI_PIPELINE_ID');
    describeBooleanProperty(vars => vars.pipeline.triggered, 'CI_PIPELINE_TRIGGERED');
    describeNumericProperty(vars => vars.project.id, 'CI_PROJECT_ID');
    describeStringProperty(vars => vars.project.name, 'CI_PROJECT_NAME');
    describeStringProperty(vars => vars.project.namespace, 'CI_PROJECT_NAMESPACE');
    describeStringProperty(vars => vars.project.path, 'CI_PROJECT_PATH');
    describeStringProperty(vars => vars.project.url, 'CI_PROJECT_URL');
    describeStringProperty(vars => vars.project.dir, 'CI_PROJECT_DIR');
    describeStringProperty(vars => vars.project.repo, 'CI_REPOSITORY_URL');
    describeOptionalSection(vars => vars.registry, 'CI_REGISTRY', () => {
      describeStringProperty(vars => (vars.registry as any).registry, 'CI_REGISTRY', false);
      describeStringProperty(vars => (vars.registry as any).image, 'CI_REGISTRY_IMAGE');
    });
    describeOptionalSection(vars => vars.environment, 'CI_ENVIRONMENT_NAME', () => {
      describeStringProperty(vars => (vars.environment as any).name, 'CI_ENVIRONMENT_NAME', false);
      describeStringProperty(vars => (vars.environment as any).slug, 'CI_ENVIRONMENT_SLUG');
    });
    describeNumericProperty(vars => vars.runner.id, 'CI_RUNNER_ID');
    describeStringProperty(vars => vars.runner.description, 'CI_RUNNER_DESCRIPTION');
    describe('runner.tags', () => {
      it('is empty if CI_RUNNER_TAGS is empty.', () => {
        env.CI_RUNNER_TAGS = '';
        expect(getVariables().runner.tags).to.be.empty;
      });
      it('is empty if CI_RUNNER_TAGS is undefined.', () => {
        expect(getVariables().runner.tags).to.be.empty;
      });
      it('is a list of the comma-seperated values of CI_RUNNER_TAGS.', () => {
        env.CI_RUNNER_TAGS = 'abc, 123DS,foo';
        expect(getVariables().runner.tags).to.deep.equal(['abc', '123DS', 'foo']);
      });
    });
    describeBooleanProperty(vars => vars.debug, 'CI_DEBUG_TRACE');
    describeNumericProperty(vars => vars.user.id, 'GITLAB_USER_ID');
    describeStringProperty(vars => vars.user.email, 'GITLAB_USER_EMAIL');
  });
});

describe('load() Functional', () => {
  before(function (this: { skip: () => void }): void {
    if (process.env.GITLAB_CI !== 'true') {
      // tslint:disable-next-line:no-invalid-this
      this.skip();
    }
  });

  it('should return a representation of the value from the environment', () => {
    const variables = loadVariables();
    if (variables === undefined) {
      throw new Error('Expected loadVariables() to parse the variables.');
    }

    expect(variables.commit.sha).to.be.match(/^[0-9a-f]{40}$/).and.equal(process.env.CI_COMMIT_SHA);
    expect(variables.commit.ref).to.be.a('string').and.equal(process.env.CI_COMMIT_REF_NAME);
    expect(variables.commit.refSlug).to.be.a('string').and.equal(process.env.CI_COMMIT_REF_SLUG);
    expect(variables.commit.tag).to.equal(process.env.CI_COMMIT_TAG);

    expect(variables.job.id).to.be.a('number').and.equal(process.env.CI_JOB_ID as any * 1);
    expect(variables.job.manual).to.be.a('boolean');
    expect(variables.job.name).to.equal('test:mocha'); // From .gitlab-ci.yml
    expect(variables.job.stage).to.equal('test');
    expect(variables.job.token).to.be.a('string').equal(process.env.CI_JOB_TOKEN);

    expect(variables.debug).to.be.false;

    expect(variables.pipeline.id).to.be.a('number').and.equal(process.env.CI_PIPELINE_ID as any * 1);
    expect(variables.pipeline.triggered).to.be.a('boolean');

    expect(variables.project.dir).to.equal('/builds/janslow/gitlab-ci-variables');
    expect(variables.project.id).to.equal(1940191);
    expect(variables.project.name).to.equal('gitlab-ci-variables');
    expect(variables.project.namespace).to.equal('janslow');
    expect(variables.project.path).to.equal('janslow/gitlab-ci-variables');
    expect(variables.project.url).to.equal('https://gitlab.com/janslow/gitlab-ci-variables');
    expect(variables.project.repo).to.be.a('string').and.equal(process.env.CI_REPOSITORY_URL);

    expect((variables.registry as any).registry).to.equal('registry.gitlab.com');
    expect((variables.registry as any).image).to.equal(undefined);

    if (process.env.CI_ENVIRONMENT_NAME) {
      expect((variables.environment as any).name).to.equal(process.env.CI_ENVIRONMENT_NAME);
      expect((variables.environment as any).slug).to.equal(process.env.CI_ENVIRONMENT_SLUG);
    }
    else {
      expect(variables.environment).to.be.undefined;
    }

    expect(variables.runner.id).to.be.a('number').and.equal(process.env.CI_RUNNER_ID as any * 1);
    expect(variables.runner.description).to.be.a('string').and.equal(process.env.CI_RUNNER_DESCRIPTION);
    expect(variables.runner.tags).to.include('docker');
    expect(variables.runner.tags.join(', ')).to.equal(process.env.CI_RUNNER_TAGS);

    expect(variables.server.name).to.equal('GitLab');
    expect(variables.server.revision).to.be.a('string').and.equal(process.env.CI_SERVER_REVISION);
    expect(variables.server.version).to.be.a('string').and.equal(process.env.CI_SERVER_VERSION);

    expect(variables.user.id).to.be.a('number').and.equal(process.env.GITLAB_USER_ID as any * 1);
    expect(variables.user.email).to.match(/@/).and.equal(process.env.GITLAB_USER_EMAIL);
  });
});
