# Changelog

## develop
* [breaking] Require GitLab 9.0.
* [breaking] Remove `Variables#build`.
  - Split across `#commit`, `#job`, `#pipeline` and `#project`.

## 0.1.0
* Initial release
